#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018


"""Module for computing the antenna parameter."""
import ephem
from datetime import timedelta, date, datetime
from ftplib import FTP
import configparser
import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
from xml.dom import minidom
import shutil

# Load Config
config = configparser.ConfigParser()
config.read('config_development.ini')

# Set Schedule Directory
scheduleDirectory = str(config['LOCAL']['SCHEDULE_PATH'])
tlePathDirectory = str(config['LOCAL']['TLE_PATH'])
tleOldPathDirectory = str(config['LOCAL']['TLE_OLD_PATH'])

# Set Station Position
stationLat = config['THSR']['LATITUDE']
stationLng = config['THSR']['LONGITUDE']
stationAlt = float(config['THSR']['ALTITUDE'])


class Antenna:

    # Load Schedule from Local Storage
    def loadSchedule(self):
        print("MPP Generator Starting...\n")
        print("[ Load Schedule ]")
        self.scheduleList = []

        for file in glob.glob(os.path.join(scheduleDirectory, '*.asc')):
            print(" + " + file)
            self.scheduleList.append(file)

        sorted(self.scheduleList)


        print("Found: %d Files" % len(self.scheduleList))
        print("============================================\n")

    # Parsing Schedule Filename
    def parserSchedule(self):
        for idx, file in enumerate(self.scheduleList):
            print("[ Schedule #%d ]" % (idx + 1))
            self.originalScheduleFileName = os.path.basename(file)
            self.splitScheduleFileName = self.originalScheduleFileName.split("_")
            self.satelliteName = self.splitScheduleFileName[0]
            self.stationName = self.splitScheduleFileName[1]
            print(" + %s" % self.satelliteName)
            print(" + %s" % self.stationName)
            print("============================================\n")
            self.newPass = []
            self.deletePass = []
            Antenna.parsingSchedulePass(self, file)
            originalFile = os.path.basename(file)
        # shutil.move(config['LOCAL']['SCHEDULE_PATH']+originalFile, config['LOCAL']['SCHEDULE_OLD_PATH']+originalFile)

    # Parsing Schedule Pass Data
    def parsingSchedulePass(self, file):
        self.schedulePassResponse = []
        print("[ Check Schedule Pass ]")
        print("File: %s" % file)
        schedulePass = []
        f = open(file, 'r')
        content = f.read()
        f.close()
        for schedule in content.splitlines():
            tmp = schedule.split(",")
            schedulePass.append(schedule)
            print(" + " + schedule)
        print("Found: %d Pass" % len(schedulePass))
        print("============================================\n")

        # Call Function to Propagate Azimuth and Elevation
        for idx, schedule in enumerate(schedulePass):
            Antenna.satellitePropagator(self, idx + 1, schedule, self.satelliteName)

        # Call Function to Create and Replace Pass
        self.writeNewPass()

        # Call Function to Delete Pass
        self.deleteNewPass()

        # Call Function to Create Pass Response File
        self.createPassResponseFile()

        # Call Function to Move TLE to OLD Directory
        self.moveTLE()

    # Create and Replace Pass
    def writeNewPass(self):
        print("New Pass Length: %d" % len(self.newPass))
        for idx, data in enumerate(self.newPass):

            
            print('Writing Pass...')
            # print(data[0][0])
            # print(data[0][1])
            MPPFile = None
            startDateMPP = data[0][0][:8]
            endDateMPP = data[0][1][:8]
            if (len(glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDateMPP + '*'))) == 0):
                # print(startDateMPP)
                self.ftp = FTP(config['MPP']['HOST'], config['MPP']['USER'], config['MPP']['PASS'])
                self.ftp.cwd('/')
                self.ftp.cwd(config['MPP']['MPP_PATH'])
                for filename in self.ftp.nlst('MPP_' + startDateMPP + '*.xml'):
                    print("Downloading: %s (%d Byte)" % (filename, self.ftp.size(filename)))
                    self.ftp.retrbinary("RETR " + filename,
                                        open(os.path.join(config['LOCAL']['MPP_PATH'], filename), 'wb').write)

                    self.ftp.rename(filename, config['MPP']['MPP_OLD_PATH'] + filename)

            originalPass = []

            for file in glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDateMPP + '*')):
                print('Found MPP: ' + file)
                tree = ET.parse(file)
                Mission_Pass_Plan = tree.getroot()
                DefaultHeader = Mission_Pass_Plan.find('Header')
                DefaultFilename = DefaultHeader.find('Filename').text
                DefaultProduction_time = DefaultHeader.find('Production_time').text
                DefaultStart_validity_time = DefaultHeader.find('Start_validity_time').text

                for Passes in Mission_Pass_Plan.findall('Passes'):
                    tmpPass = []
                    for Pass in Passes.findall('Pass'):

                        AOS_time = Pass.find('AOS_time').text
                        LOS_time = Pass.find('LOS_time').text
                        # print(AOS_time)
                        # print(LOS_time)
                        Orbital_data = Pass.find('Orbital_data')
                        PointArray = []
                        for Point in Orbital_data.findall('Point'):
                            Az = Point.find('Az').text
                            El = Point.find('El').text
                            Time = Point.find('Time').text
                            PointArray.append([Az, El, Time])

                        tmpPass.append([AOS_time, LOS_time, PointArray])
                    originalPass.append(tmpPass)
                if os.path.exists(file):
                    os.remove(file)

            print('Original Pass Length: %d' % len(originalPass))

            mergePass = []
            for original in originalPass:
                mergePass = mergePass + original
            mergePass = mergePass + data
            print(len(mergePass))
            mergePass.sort(key=lambda x: x[0])

            print('Merge Pass Length: %d' % len(mergePass))

            # print('Merging MPP File...')
            # self.mergeMPP(data[0][0], data[0][1], mergePass)

            print('Chunking...')
            print('Merging...')
            for ndx, c in enumerate(self.chunks(mergePass, 8)):
                self.mergeMPP(c)

    # Delete Pass
    def deleteNewPass(self):
        print("Delete Pass Length: %d" % len(self.deletePass))
        for idx, data in enumerate(self.deletePass):
            print('Writing Pass...')
            MPPFile = None
            startDateMPP = data[0][0][:8]
            endDateMPP = data[0][1][:8]
            if (len(glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDateMPP + '*'))) == 0):
                print(startDateMPP)
                self.ftp = FTP(config['MPP']['HOST'], config['MPP']['USER'], config['MPP']['PASS'])
                self.ftp.cwd('/')
                self.ftp.cwd(config['MPP']['MPP_PATH'])
                for filename in self.ftp.nlst('MPP_' + startDateMPP + '*.xml'):
                    print("Downloading: %s (%d Byte)" % (filename, self.ftp.size(filename)))
                    self.ftp.retrbinary("RETR " + filename,
                                        open(os.path.join(config['LOCAL']['MPP_PATH'], filename), 'wb').write)

                    self.ftp.rename(filename, config['MPP']['MPP_OLD_PATH'] + filename)

            originalPass = []

            for file in glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDateMPP + '*')):
                print('Found MPP: ' + file)
                tree = ET.parse(file)
                Mission_Pass_Plan = tree.getroot()
                DefaultHeader = Mission_Pass_Plan.find('Header')
                DefaultFilename = DefaultHeader.find('Filename').text
                DefaultProduction_time = DefaultHeader.find('Production_time').text
                DefaultStart_validity_time = DefaultHeader.find('Start_validity_time').text

                for Passes in Mission_Pass_Plan.findall('Passes'):
                    tmpPass = []
                    for Pass in Passes.findall('Pass'):

                        AOS_time = Pass.find('AOS_time').text
                        LOS_time = Pass.find('LOS_time').text

                        if data[0] != AOS_time and data[1] != LOS_time:
                            # print(AOS_time)
                            # print(LOS_time)
                            Orbital_data = Pass.find('Orbital_data')
                            PointArray = []
                            for Point in Orbital_data.findall('Point'):
                                Az = Point.find('Az').text
                                El = Point.find('El').text
                                Time = Point.find('Time').text
                                PointArray.append([Az, El, Time])

                            tmpPass.append([AOS_time, LOS_time, PointArray])
                            # print('Save Pass')
                    originalPass.append(tmpPass)
                if os.path.exists(file):
                    os.remove(file)

            print('Original Pass Length: %d' % len(originalPass[0]))

            originalPass[0].sort(key=lambda x: x[0])

            # print('Merging MPP File...')
            # self.mergeMPP(data[0][0], data[0][1], mergePass)

            print('Chunking...')
            print('Merging...')
            for ndx, c in enumerate(self.chunks(originalPass[0], 8)):
                self.mergeMPP(c)
                # print(ndx)

    # Merge MPP
    def mergeMPP(self, mergePass):
        
        # print(mergePass[0][0])
        startDate = mergePass[0][0]
        endDate = mergePass[0][1]

        Mission_Pass_Plan = ET.Element('Mission_Pass_Plan')
        Header = ET.SubElement(Mission_Pass_Plan, 'Header')
        Satellite_id = ET.SubElement(Mission_Pass_Plan, 'Satellite_id')
        Satellite_id.text = config['MAINSATELLITE']['SATELLITE']
        Pass_number = ET.SubElement(Mission_Pass_Plan, 'Pass_number')
        Pass_number.text = str(len(mergePass))
        Passes = ET.SubElement(Mission_Pass_Plan, 'Passes')



        for idx, tmpPass in enumerate(mergePass):
            # print(idx)
            if (tmpPass[2]):
                # startDate = datetime.today().strftime('%Y%m%d%H%M%S%f')[:-3]
                # startDate = startDate - timedelta(days=1)

                if tmpPass[2][0][2] < startDate:
                    startDate = tmpPass[2][0][2]

                if tmpPass[2][len(tmpPass[2]) - 1][2] > endDate:
                    endDate = tmpPass[2][len(tmpPass[2]) - 1][2]

                Pass = ET.Element('Pass')
                Orbit_number = ET.SubElement(Pass, 'Orbit_number')
                Orbit_number.text = str(idx + 1)
                AOS_time = ET.SubElement(Pass, 'AOS_time')
                AOS_time.text = tmpPass[2][0][2]
                LOS_time = ET.SubElement(Pass, 'LOS_time')
                LOS_time.text = tmpPass[2][len(tmpPass[2]) - 1][2]
                Orbital_data = ET.SubElement(Pass, 'Orbital_data')

                TC = ET.SubElement(Pass, 'TC')
                Polar = ET.SubElement(TC, 'Polar')
                Polar.text = 'RHCP'
                EIRP = ET.SubElement(TC, 'EIRP')
                EIRP.text = '43'

                TM = ET.SubElement(Pass, 'TM')
                Doppler_meas = ET.SubElement(TM, 'Doppler_meas')
                Doppler_meas.text = 'ON'
                Doppler_sample_rate = ET.SubElement(TM, 'Doppler_sample_rate')
                Doppler_sample_rate.text = '1'
                Storage = ET.SubElement(TM, 'Storage')
                Storage.text = 'ON'
                TM_file = ET.SubElement(TM, 'TM_file')
                TM_file.text = '1'

                for Point in tmpPass[2]:
                    PointArray = ET.Element('Point')
                    Az = ET.SubElement(PointArray, 'Az')
                    Az.text = str(Point[0])
                    El = ET.SubElement(PointArray, 'El')
                    El.text = str(Point[1])
                    Time = ET.SubElement(PointArray, 'Time')
                    Time.text = str(Point[2])
                    Orbital_data.append(PointArray)
                Passes.append(Pass)

        Filename = ET.SubElement(Header, 'Filename')
        Production_time = ET.SubElement(Header, 'Production_time')
        Start_validity_time = ET.SubElement(Header, 'Start_validity_time')
        Filename.text = 'MPP_' + startDate + '_' + endDate
        Production_time.text = startDate

        validityTime = datetime.strptime(startDate, '%Y%m%d%H%M%S%f')
        validityTime = validityTime - timedelta(minutes=15)
        validityTime =  str(validityTime.strftime('%Y%m%d%H%M%S%f')[:-3])
        Start_validity_time.text = validityTime

        data = minidom.parseString(ET.tostring(Mission_Pass_Plan)).toprettyxml(indent="   ")
        print("Writing File: %s" % str('MPP_'+startDate+'_'+validityTime+'.xml'))
        # print(data)

        with open(config['LOCAL']['MPP_PATH'] + 'MPP_'+startDate+'_'+validityTime+'.xml', "w") as f:
            f.write(data)

        print('---------------------------------------')

    # Create Response File
    def createPassResponseFile(self):
        self.splitScheduleFileName.append(self.splitScheduleFileName[4])
        self.splitScheduleFileName[4] = 'Response'
        joinSchedulePassResponse = "_".join(self.splitScheduleFileName)
        responsePassFile = open(config['LOCAL']['SCHEDULE_RESPONSE_PATH'] + joinSchedulePassResponse, 'w')
        for response in self.schedulePassResponse:
            responsePassFile.write("%s\n" % response)
        responsePassFile.close()

    # Check Conflict Pass from Exists File
    def checkConflict(self, defaultPass, AOSScheduleTime, LOSScheduleTime):
        print("[ Checking Conflict ]")
        Conflict = 0
        Available = 0
        for data in defaultPass:
            AOS = data[0]
            LOS = data[1]
            # print(AOS, LOS, AOSScheduleTime, LOSScheduleTime)
            if (AOS == None or LOS == None):
                Available = Available + 1
            if ((LOSScheduleTime >= AOS and LOSScheduleTime <= LOS)
                    or (AOSScheduleTime >= AOS and AOSScheduleTime <= LOS)
                    or (AOSScheduleTime >= AOS and LOSScheduleTime <= LOS)
                    or (AOSScheduleTime <= AOS and LOSScheduleTime >= LOS)
            ):
                Conflict = Conflict + 1
            else:
                Available = Available + 1
        # print(Conflict, Available)
        if Conflict > 0:
            return True
        else:
            return False

    # Satellite Propagator (Azimuth, Elevation, Time)
    def satellitePropagator(self, idx, schedulePass, satelliteName):
        print("[ Pass #%d  ]" % (idx))
        splitSchedulePassResponse = schedulePass.split(",")
        splitSchedulePass = schedulePass.split(",")


        AOSDate = splitSchedulePass[3].split("-")
        AOSDOY = splitSchedulePass[4]
        AOSTime = splitSchedulePass[5].split(":")
        LOSDate = splitSchedulePass[6].split("-")
        LOSDOY = splitSchedulePass[7]
        LOSTime = splitSchedulePass[8].split(":")

        startDatetime = datetime(int(AOSDate[0]), int(AOSDate[1]), int(AOSDate[2]), int(AOSTime[0]), int(AOSTime[1]),
                                 int(AOSTime[2]), 0)
        endDatetime = datetime(int(LOSDate[0]), int(LOSDate[1]), int(LOSDate[2]), int(LOSTime[0]), int(LOSTime[1]),
                               int(LOSTime[2]), 0)
        startDate = startDatetime.strftime('%Y-%m-%d')
        endDate = endDatetime.strftime('%Y-%m-%d')

        print("Mode: %s" % splitSchedulePass[2])
        print("Start: %s" % startDatetime)
        print("End: %s" % endDatetime)

        AOSScheduleTime = startDatetime.strftime('%Y%m%d%H%M%S%f')[:-3]
        LOSScheduleTime = endDatetime.strftime('%Y%m%d%H%M%S%f')[:-3]

        if (splitSchedulePass[2] == 'delete'):
            print("Delete Pass")
            self.deletePass.append([AOSScheduleTime, LOSScheduleTime])
            return

        # Load TLE by Date
        TleFile = Antenna.loadTLE(self, self.satelliteName, self.stationName, str(startDate))

        print(TleFile)
        if (TleFile):

            # newPass = []
            passData = []

            # Load MPP
            ExistMPP = Antenna.loadExistMPP(self, startDatetime)
            print("[ Loading MPP ]")
            MPPName = ExistMPP[0]
            defaultPass = ExistMPP[1]


            # Check Conflict Pass
            checkConflict = Antenna.checkConflict(self, defaultPass, AOSScheduleTime, LOSScheduleTime)

            if (checkConflict == True):
                splitSchedulePassResponse[2] = 'Conflict'
                joinSchedulePassResponse = ",".join(splitSchedulePassResponse)
                self.schedulePassResponse.append(joinSchedulePassResponse)
                print("Status: Conflict")
            else:
                splitSchedulePassResponse[2] = 'Scheduled'
                joinSchedulePassResponse = ",".join(splitSchedulePassResponse)
                self.schedulePassResponse.append(joinSchedulePassResponse)
                print("Status: Scheduled")

                # Calculate Date Range
                prng = pd.date_range(startDatetime, endDatetime, freq='1000L')

                with open(TleFile) as f:
                    content = f.readlines()
                # you may also want to remove whitespace characters like `\n` at the end of each line
                content = [x.strip() for x in content]
                # print(content)
                # print(content[0])
                # print(content[1])
                # print(content[2])
                j = ephem.readtle(content[0], content[1], content[2])
                PointArray = []

                for timeData in prng:
                    # print('test')
                    lowell = ephem.Observer()
                    lowell.lon = stationLng
                    lowell.lat = stationLat
                    lowell.elevation = stationAlt
                    lowell.date = timeData

                    j.compute(lowell)

                    alt = j.alt
                    az = j.az
                    alt = str(alt).split(':')
                    az = str(az).split(':')
                    alt = self.dms_to_dd(alt[0], alt[1], alt[2])
                    az = self.dms_to_dd(az[0], az[1], az[2])

                    Az = str(round(az, 3))
                    El = str(round(alt, 3))
                    Time = timeData.strftime('%Y%m%d%H%M%S%f')[:-3]

                    if (float(alt) >= 5 and float(alt) <= 90):
                        PointArray.append([Az, El, Time])

                passData.append([AOSScheduleTime, LOSScheduleTime, PointArray])

                self.newPass.append(passData)
                # Antenna.combineMPP(self, MPPName, defaultPass, newPass)

        print("======================\n")

    # Convert DMS to Decimal Degree
    def dms_to_dd(self, d, m, s):
        dd = float(d) + float(m) / 60 + float(s) / 3600
        return dd

    # Chunking List
    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i + n]

    # Combine MPP
    def combineMPP(self, MPPName, defaultPass, newPass):

        start_date = datetime.strptime(newPass[0][0], '%Y%m%d%H%M%S%f')
        start_date = start_date.strftime('%Y%m%d')

        if (MPPName):

            mergePass = defaultPass + newPass

            print("[ Combine Pass ]")
            print("Current Pass Count: %d" % len(mergePass))
            print("Exist MPP: " + os.path.basename(MPPName))
            fileNameMPP = os.path.basename(MPPName)

            Mission_Pass_Plan = ET.Element('Mission_Pass_Plan')
            Header = ET.SubElement(Mission_Pass_Plan, 'Header')
            Filename = ET.SubElement(Header, 'Filename')
            Production_time = ET.SubElement(Header, 'Production_time')
            Start_validity_time = ET.SubElement(Header, 'Start_validity_time')
            Filename.text = fileNameMPP
            Production_time.text = datetime.now().strftime('%Y%m%d%H%M%S%f')[:-3]
            Start_validity_time.text = datetime.now().strftime('%Y%m%d%H%M%S%f')[:-3]
            Satellite_id = ET.SubElement(Mission_Pass_Plan, 'Satellite_id')
            Satellite_id.text = config['MAINSATELLITE']['SATELLITE']
            Pass_number = ET.SubElement(Mission_Pass_Plan, 'Pass_number')
            Pass_number.text = str(len(mergePass))
            Passes = ET.SubElement(Mission_Pass_Plan, 'Passes')

            # Sort Pass
            mergePass.sort(key=lambda x: x[0])

            for idx, tmpPass in enumerate(mergePass):
                print(tmpPass[0])
                if (tmpPass[2]):
                    Pass = ET.Element('Pass')
                    Orbit_number = ET.SubElement(Pass, 'Orbit_number')
                    Orbit_number.text = str(idx + 1)
                    AOS_time = ET.SubElement(Pass, 'AOS_time')
                    AOS_time.text = tmpPass[2][0][2]
                    LOS_time = ET.SubElement(Pass, 'LOS_time')
                    LOS_time.text = tmpPass[2][len(tmpPass[2]) - 1][2]
                    Orbital_data = ET.SubElement(Pass, 'Orbital_data')

                    TC = ET.SubElement(Pass, 'TC')
                    Polar = ET.SubElement(TC, 'Polar')
                    Polar.text = 'RHCP'
                    EIRP = ET.SubElement(TC, 'EIRP')
                    EIRP.text = '43'

                    TM = ET.SubElement(Pass, 'TM')
                    Doppler_meas = ET.SubElement(TM, 'Doppler_meas')
                    Doppler_meas.text = 'ON'
                    Doppler_sample_rate = ET.SubElement(TM, 'Doppler_sample_rate')
                    Doppler_sample_rate.text = '1'
                    Storage = ET.SubElement(TM, 'Storage')
                    Storage.text = 'ON'
                    TM_file = ET.SubElement(TM, 'TM_file')
                    TM_file.text = '1'

                    for Point in tmpPass[2]:
                        PointArray = ET.Element('Point')
                        Az = ET.SubElement(PointArray, 'Az')
                        Az.text = str(Point[0])
                        El = ET.SubElement(PointArray, 'El')
                        El.text = str(Point[1])
                        Time = ET.SubElement(PointArray, 'Time')
                        Time.text = str(Point[2])
                        Orbital_data.append(PointArray)
                    Passes.append(Pass)

            data = minidom.parseString(ET.tostring(Mission_Pass_Plan)).toprettyxml(indent="   ")
            print("Writing File...")
            # print(data)

            with open(config['LOCAL']['MPP_PATH'] + fileNameMPP, "w") as f:
                f.write(data)

        print("======================\n")

    # Load Exist MPP File
    def loadExistMPP(self, startDate):
        print("[ Load Exist MPP ]")

        # startDate = startDate - timedelta(days=1)
        endDate = startDate + timedelta(days=1)

        startDateMPP = startDate.strftime('%Y%m%d%H%M%S%f')[:-3]
        endDateMPP = endDate.strftime('%Y%m%d%H%M%S%f')[:-3]

        startDate = startDate.strftime('%Y%m%d')
        print('MPP Date: %s' % startDate)
        MPPFile = None
        if (len(glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDate + '*.xml'))) == 0):
            self.ftp = FTP(config['MPP']['HOST'], config['MPP']['USER'], config['MPP']['PASS'])
            self.ftp.cwd('/')
            self.ftp.cwd(config['MPP']['MPP_PATH'])
            for filename in self.ftp.nlst('MPP_' + startDate + '*.xml'):
                print("Downloading: %s (%d Byte)" % (filename, self.ftp.size(filename)))
                self.ftp.retrbinary("RETR " + filename,
                                    open(os.path.join(config['LOCAL']['MPP_PATH'], filename), 'wb').write)

                self.ftp.rename(filename, config['MPP']['MPP_OLD_PATH'] + filename)

        for file in glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDate + '*.xml')):
            print('Found MPP: ' + file)
            MPPFile = file

        defaultPass = []
        if (MPPFile):
            tree = ET.parse(MPPFile)
            Mission_Pass_Plan = tree.getroot()
            DefaultHeader = Mission_Pass_Plan.find('Header')
            DefaultFilename = DefaultHeader.find('Filename').text
            DefaultProduction_time = DefaultHeader.find('Production_time').text
            DefaultStart_validity_time = DefaultHeader.find('Start_validity_time').text

            for Passes in Mission_Pass_Plan.findall('Passes'):
                for Pass in Passes.findall('Pass'):
                    AOS_time = Pass.find('AOS_time').text
                    LOS_time = Pass.find('LOS_time').text
                    Orbital_data = Pass.find('Orbital_data')
                    PointArray = []
                    for Point in Orbital_data.findall('Point'):
                        Az = Point.find('Az').text
                        El = Point.find('El').text
                        Time = Point.find('Time').text
                        PointArray.append([Az, El, Time])

                    defaultPass.append([AOS_time, LOS_time, PointArray])
        # else:
        #     print('MPP does not exists.')
        #
        #     fileNameMPP = os.path.basename('MPP_%s_%s.xml' % (str(startDateMPP), str(endDateMPP)))
        #
        #     Mission_Pass_Plan = ET.Element('Mission_Pass_Plan')
        #     Header = ET.SubElement(Mission_Pass_Plan, 'Header')
        #     Filename = ET.SubElement(Header, 'Filename')
        #     Production_time = ET.SubElement(Header, 'Production_time')
        #     Start_validity_time = ET.SubElement(Header, 'Start_validity_time')
        #     Filename.text = fileNameMPP
        #     Production_time.text = startDateMPP
        #     Start_validity_time.text = endDateMPP
        #     Satellite_id = ET.SubElement(Mission_Pass_Plan, 'Satellite_id')
        #     Satellite_id.text = 'THEOS-1'
        #     Pass_number = ET.SubElement(Mission_Pass_Plan, 'Pass_number')
        #     Pass_number.text = '0'
        #     Passes = ET.SubElement(Mission_Pass_Plan, 'Passes')
        #
        #     data = minidom.parseString(ET.tostring(Mission_Pass_Plan)).toprettyxml(indent="   ")
        #     print("Writing File...")
        #     # print(data)
        #
        #     with open(config['LOCAL']['MPP_PATH'] + fileNameMPP, "w") as f:
        #         f.write(data)
        #
        #     for file in glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], 'MPP_' + startDate + '*.xml')):
        #         print('Found MPP: ' + file)
        #         MPPFile = file

        print("======================\n")

        return [MPPFile, defaultPass]

    # Load TLE File
    def loadTLE(self, satelliteName, stationName, start_date):
        self.TLECollection = []
        print("[ Load TLE ]")
        print(satelliteName, stationName, start_date)
        TleFile = ''
        # for file in glob.glob(os.path.join(tlePathDirectory, satelliteName + '*' + start_date + '*.asc')):
        for file in glob.glob(os.path.join(tlePathDirectory, satelliteName + '*.asc')):
            self.originalTLEFileName = os.path.basename(file)
            self.splitTLEFileName = self.originalTLEFileName.split("_")
            TleFile = file
            print("Found File: %s" % TleFile)
            self.TLECollection.append(file)

        if len(self.TLECollection) == 0:
            print('Not found TLE')
            for file in glob.glob(os.path.join(tleOldPathDirectory, satelliteName + '*.asc')):
                self.originalTLEFileName = os.path.basename(file)
                self.splitTLEFileName = self.originalTLEFileName.split("_")
                TleFile = file
                print("Found File: %s" % TleFile)
                self.TLECollection.append(file)

        print("============================================\n")
        return TleFile

    # Move TLE to Old Directory
    def moveTLE(self):
        for file in self.TLECollection:
            try:
                shutil.move(config['LOCAL']['TLE_PATH'] + os.path.basename(file),
                            config['LOCAL']['TLE_OLD_PATH'] + os.path.basename(file))
            except Exception as e:
                print('Found Old TLE')
