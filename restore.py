#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018

# Compile on Python 3.6
# Require Library:
# - pyorbital
# - pandas
# - numpy
# - ftplib
# - configparser

"""Module for computing the antenna parameter."""

import time
import configparser
from ssc import SSC

start_time = time.time()

# Load Config
config = configparser.ConfigParser()
config.read('config_development.ini')

# Download TLE and Schedule from SSC FTP
SSC = SSC()
SSC.connectFTP()
SSC.restore()
SSC.disconnectFTP()

print("Status: Completed")
print("Computation Time: %s seconds" % (time.time() - start_time))

