import ephem
from datetime import timedelta, date, datetime
from ftplib import FTP
import configparser
import os
import glob
import pandas as pd
import xml.etree.ElementTree as ET
from xml.dom import minidom
import shutil
import math
from pprint import pprint

startDatetime = datetime(2019, 1, 17, 0, 0, 0, 0)
endDatetime = datetime(2019, 1, 27, 23, 59, 29, 29)

print(startDatetime)

# Calculate Date Range
prng = pd.date_range(startDatetime, endDatetime, freq='60000L')

name = 'THAICOM 8'
tle1 = '1 41552U 16031A   19016.46078521 -.00000111 +00000-0 +00000-0 0  9992'
tle2 = '2 41552 000.0736 055.7299 0003521 273.7400 030.5029 01.00272403009564'

j = ephem.readtle(name, tle1, tle2)

PointArray = []


def dump(obj):
    for attr in dir(obj):
        print("obj.%s = %r" % (attr, getattr(obj, attr)))


def gstimeInternal(jdut1):
    twoPi = 2 * math.pi
    deg2rad = math.pi / 180.0
    tut1 = (jdut1 - 2451545.0) / 36525.0;
    temp = (-6.2e-6 * tut1 * tut1 * tut1) + (0.093104 * tut1 * tut1) + (
                ((876600.0 * 3600) + 8640184.812866) * tut1) + 67310.54841
    temp = ((temp * deg2rad) / 240.0) % twoPi

    if temp < 0.0:
        temp += 2 * twoPi

    return temp


def geodeticToEcf(longitude, latitude, height):

    a = 6378.137
    b = 6356.7523142
    f = (a - b) / a
    e2 = ((2 * f) - (f * f))
    normal = a / math.sqrt(1 - (e2 * (math.sin(latitude) * math.sin(latitude))))

    x = (normal + height) * math.cos(latitude) * math.cos(longitude)
    y = (normal + height) * math.cos(latitude) * math.sin(longitude)
    z = ((normal * (1 - e2)) + height) * math.sin(latitude)

    return x, y, z


def ecfToEci(x, y, z, gmst):
    X = (x * math.cos(gmst)) - (y * math.sin(gmst))
    Y = (x * (math.sin(gmst))) + (y * math.cos(gmst))
    Z = z
    return X, Y, Z


def dms_to_dd(d, m, s):
    dd = float(d) + float(m) / 60 + float(s) / 3600
    return dd

time = 0
for timeData in prng:
    j.compute(timeData)

    # print(j)

    # print(j.x, j.y,j.z)
    lon2 = str(j.sublong).split(':')
    lat2 = str(j.sublat).split(':')
    lon2 = dms_to_dd(lon2[0], lon2[1], lon2[2])
    lat2 = dms_to_dd(lat2[0], lat2[1], lat2[2])
    # print(timeData)
    # print(j.sublong, j.sublat, j.elevation)
    ecf = geodeticToEcf(ephem.degrees(j.sublong), ephem.degrees(j.sublat), j.elevation)
    # print(ecf)
    julian_date = ephem.julian_date(timeData)
    # print(julian_date)
    gstime = gstimeInternal(julian_date)
    eci = ecfToEci(ecf[0], ecf[1], ecf[2], gstime)
    print('%d, %f, %f, %f,' % (time, eci[0], eci[1], eci[2]))
    time = time + 600
    # print('-------------------------')
    # print(gstime)
    # print(timeData)
#     # print('test')
#     lowell = ephem.Observer()
# lowell.lon = stationLng
# lowell.lat = stationLat
# lowell.elevation = stationAlt
# lowell.date = timeData
#
# j.compute(lowell)
#
# alt = j.alt
# az = j.az
# alt = str(alt).split(':')
#     az = str(az).split(':')
#     alt = self.dms_to_dd(alt[0], alt[1], alt[2])
#     az = self.dms_to_dd(az[0], az[1], az[2])
#
#     Az = str(round(az, 3))
#     El = str(round(alt, 3))
#     Time = timeData.strftime('%Y%m%d%H%M%S%f')[:-3]
#
#     if (float(alt) >= 5 and float(alt) <= 90):
#         PointArray.append([Az, El, Time])
# Convert DMS to Decimal Degree
