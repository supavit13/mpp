#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018

"""Module for computing the antenna parameter."""

from ftplib import FTP
import configparser
import os
import glob
import shutil

# Load Config
config = configparser.ConfigParser()
config.read('config_development.ini')

class SSC:

	# Connect to FTP
	def __init__(self):
		self.ftp = FTP(config['FTP']['HOST'], config['FTP']['USER'], config['FTP']['PASS'])

	def connectFTP(self):
		print("[ Connect FTP ]")
		print("FTP is connecting...")
		try:
			self.ftp = FTP(config['FTP']['HOST'], config['FTP']['USER'], config['FTP']['PASS'])
			self.ftp.set_pasv(False)
			print("FTP is connected !")
		except Exception as e:
			print("Connection Failed !")
		print ("============================================\n")

	# Download FTP to local storage
	def downloadTLE(self):
		print("[ Download TLE ]")
		try:
			self.ftp.cwd('/')
			self.ftp.cwd(config['FTP']['TLE_PATH'])
			self.TLECollection = []
			for filename in self.ftp.nlst('*.asc'):
				print("Downloading: %s (%d Byte)" % (filename, self.ftp.size(filename)) )
				self.ftp.retrbinary("RETR " + filename, open(os.path.join(config['LOCAL']['TLE_PATH'],filename), 'wb').write)
				self.TLECollection.append(filename)
		except Exception as e:
			print('Could not download TLE !')

		print("============================================\n")

	# Move TLE to old directory
	def moveTLE(self):
		self.ftp.cwd('/')
		self.ftp.cwd(config['FTP']['TLE_PATH'])
		print("[ Move TLE to OLD ]")
		for filename in self.TLECollection:
			print("Filename: " + filename)
			self.ftp.rename(filename, config['FTP']['TLE_OLD_PATH']+filename)
		print ("============================================\n")

	# Download Schedule
	def downloadSchedule(self):
		print("[ Download Schedule ]")
		try:
			self.ftp.cwd('/')
			self.ftp.cwd(config['FTP']['SCHEDULE_PATH'])
			self.ScheduleCollection = []
			for filename in self.ftp.nlst('*.asc'):
				print("Downloading: %s (%d Byte)" % (filename, self.ftp.size(filename)) )
				self.ftp.retrbinary("RETR " + filename, open(os.path.join(config['LOCAL']['SCHEDULE_PATH'],filename), 'wb').write)
				self.ScheduleCollection.append(filename)
		except Exception as e:
			print('Could not download Schedule !')

		print("============================================\n")

	# Move Schedule to old directory
	def moveSchedule(self):
		self.ftp.cwd('/')
		self.ftp.cwd(config['FTP']['SCHEDULE_PATH'])
		print("[ Move Schedule to OLD ]")
		for file in self.ScheduleCollection:
			self.ftp.rename(file, config['FTP']['SCHEDULE_OLD_PATH'] + file)
			try:
				print("Filename: " + file)
				shutil.move(config['LOCAL']['SCHEDULE_PATH'] + os.path.basename(file),
							config['LOCAL']['SCHEDULE_OLD_PATH'] + os.path.basename(file))
			except Exception as e:
				print('Found Old TLE')

		print("============================================\n")

	# Upload Schedule Response to FTP
	def uploadScheduleResponse(self):
		self.connectFTP()
		print("[ Upload Schedule Response ]")
		for file in glob.glob(os.path.join(config['LOCAL']['SCHEDULE_RESPONSE_PATH'], '*.asc')):
			originalFile = os.path.basename(file)
			print("Uploading: " + originalFile)
			file = open(file,'rb')   
			self.ftp.storbinary('STOR '+config['FTP']['SCHEDULE_RESPONSE_PATH']+originalFile, file)
			file.close()
			shutil.move(config['LOCAL']['SCHEDULE_RESPONSE_PATH']+originalFile, config['LOCAL']['SCHEDULE_RESPONSE_OLD_PATH']+originalFile)
		print("============================================\n")


	# Disconnect FTP
	def disconnectFTP(self):
		try:
			self.ftp.quit()
			print("FTP is disconnected")
		except Exception as e:
			print("Can't disconnect !")
		print ("============================================\n")

	# Restore File (Only Development)
	def restore(self):
		print("[ Restore TLE File ]")
		self.ftp.cwd('/')
		self.ftp.cwd(config['FTP']['TLE_PATH']+config['FTP']['TLE_OLD_PATH'])
		for filename in self.ftp.nlst('*.asc'):
			print("Moving: %s (%d Byte)" % (filename, self.ftp.size(filename)) )
			self.ftp.rename(filename, config['FTP']['TLE_PATH']+filename)

		print("[ Restore Schedule File ]")
		self.ftp.cwd('/')
		self.ftp.cwd(config['FTP']['SCHEDULE_PATH']+config['FTP']['SCHEDULE_OLD_PATH'])
		for filename in self.ftp.nlst('*.asc'):
			print("Moving: %s (%d Byte)" % (filename, self.ftp.size(filename)) )
			self.ftp.rename(filename, config['FTP']['SCHEDULE_PATH']+filename)

		print("[ Restore Local Data ]")
		for file in glob.glob(os.path.join(config['LOCAL']['TLE_PATH'], '*.asc')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['TLE_OLD_PATH'], '*.asc')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['SCHEDULE_PATH'], '*.asc')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['SCHEDULE_OLD_PATH'], '*.asc')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['SCHEDULE_RESPONSE_PATH'], '*.asc')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['SCHEDULE_RESPONSE_OLD_PATH'], '*.asc')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], '*.xml')):
			os.remove(file)
		for file in glob.glob(os.path.join(config['LOCAL']['MPP_OLD_PATH'], '*.xml')):
			os.remove(file)