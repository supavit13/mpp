## การเตรียมไฟล์ Mission pass plan
* เข้า https://sentinelasia.gistda.or.th/ 
* ไปที่แถบ Satellites & Orbit กรอกวันที่ที่จะรับและเลือกดาวเทียม จากนั้นกด compute
* สร้างไฟล์ในแฟ้ม `GK2_GISTDA/Schedule` ตั้งชื่อตามนี้ 
 `ชื่อดาวเทียม_THSR_Pass_Plan_Response_19005.1.asc` 
 เช่น 
 `SENTINEL-2B_THSR_Pass_Plan_Response_19005.1.asc`
* เพิ่มเนื้อหาในไฟล์ดังนี้ 
    ```
    ชื่อดาวเทียม,THSR,Add,วันที่เข้าจาก sentinelasia,วันที่เข้าของปี,เวลาเข้าจาก sentinelasia,วันที่ออกจาก sentinelasia,วันที่ออกของปี,เวลาออกจาก sentinelasia,,RNG
    ```
    เช่น
    ```
    SENTINEL-2B,THSR,Add,2019-05-15,134,04:03:00,2019-05-15,134,04:05:00,,RNG
    ```
    สามารถดูได้จากแถบ Feasibility Acquisition เลือกดาวเทียมที่ต้องการ
* เข้า https://www.n2yo.com/ ค้นหาดาวเทียมที่ต้องการ และคัดลอก TLE มาใส่ในขั้นตอนถัดไป
* สร้างไฟล์ในแฟ้ม `GK2_GISTDA/Orbital_Elements` ตั้งชื่อตามชื่อดาวเทียมที่เลือกเช่น `SENTINEL-2B.asc`
* เพิ่มเนื้อหาในไฟล์ดังนี้
    ```
    ชื่อดาวเทียม
    TLE บรรทัดที่ 1
    TLE บรรทัดที่ 2
    ```
    เช่น
    ```
    SENTINEL-2B
    1 42063U 17013A   19133.73907914  .00000006  00000-0  18834-4 0  9998
    2 42063  98.5679 208.7380 0001128  78.8926 281.2385 14.30817052114074
    ```
* จากนั้นเปิด terminal เพื่อรันไฟล์ `main.py`
    ```
    python main.py
    ```
## การใช้งาน
* เปิดเครื่อง G5500, Arduino และต่อ USB จาก Arduino เข้า Computer
* เปิด Serial Monitor บน Arduino เลือก buadrate เป็น 115200 และ reset arduino
* เมื่อ Serial Monitor แสดง Menu ให้เลือกเมนู 2 เพื่อรอการรับข้อมูล Mission pass plan
* นำไฟล์ output ที่อยู่ใน `GK2_GISTDA/MPP` สกุล `.xml` ไปไว้ที่ `MPPReader`
* เปิด terminal รันไฟล์ `MPPReader.py` โดยใช้คำสั่งดังนี้
    ```
    python MPPReader.py <ชื่อไฟล์>
    ```
    เช่น
    ```
    python MPPReader.py MPP_20190515040300000_20190515034800000.xml