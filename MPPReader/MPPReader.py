import json
import xmltodict
from time import sleep, time
import paho.mqtt.client as mqtt
import sys #for argument

def on_connect(client, userdata, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("G5500")
    
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def convertTime(time):
    currentTime = time[0:2] + ":" + time[2:4] + ":" + time[4:6]
    return currentTime

with open(sys.argv[1], 'r') as f:
    xmlString = f.read()
 
# print("XML input (MPP_20181230050500000_20181231050500000.xml):")
# print(xmlString)
     
jsonString = json.dumps(xmltodict.parse(xmlString), indent=4)

jsonObj = json.loads(jsonString)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.subscribe("G5500")
client.connect("soar.gistda.or.th",1883,60)

data = {}
file = open("test.txt",'w')
file = open("test.txt",'a+')
for i in jsonObj['Mission_Pass_Plan']['Passes']['Pass']['Orbital_data']['Point']:
    data['Az'] = int(round(float(i['Az'])))
    data['El'] = int(round(float(i['El'])))
    data['Time'] = convertTime(i['Time'][8:])
    raw = str(data['Az'])+','+str(data['El'])+','+data['Time']+"#"
    print(raw)
    file.write(raw+"\n")
    client.publish("setAzEl",raw)
    # sleep(0.5)

# sleep(0.5)
print("EOF")
client.publish("setAzEl","EOF")
file.write("EOF")
file.close()


    
 
# with open("output.json", 'w') as f:
#     f.write(jsonString)