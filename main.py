#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018

# Compile on Python 3.6

# Require Library:
# - ephem
# - pandas
# - numpy
# - ftplib
# - configparser


"""Module for computing the antenna parameter."""

import time
import configparser
from ssc import SSC
from mpp import MPP
from antenna import Antenna

start_time = time.time()

# Load Config
config = configparser.ConfigParser()
config.read('config_development.ini')

# Download TLE and Schedule from SSC FTP
SSC = SSC()
SSC.connectFTP()
# SSC.downloadTLE()
# SSC.downloadSchedule()

# Generate Response and MPP File
Antenna = Antenna()
Antenna.loadSchedule()
Antenna.parserSchedule()

# Upload Schedule Response to SSC FTP
# SSC.uploadScheduleResponse()

# Upload MPP to MPP FTP
# MPP = MPP()
#PP.connectFTP()
# MPP.uploadMPP()
# MPP.disconnectFTP()

# Move TLE to OLD
# SSC.moveTLE()
# SSC.moveSchedule()
SSC.disconnectFTP()

# Show computation time
print('\n- - - - - - - - - - - - - - - - - - - -')
print("Computation Time: %s seconds" % (time.time() - start_time))

