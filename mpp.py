#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2018

"""Module for computing the antenna parameter."""

from ftplib import FTP
import configparser
import os
import glob
import shutil

# Load Config
config = configparser.ConfigParser()
config.read('config_development.ini')

class MPP:

	# Connect to FTP
	def __init__(self):
		self.ftp = FTP(config['MPP']['HOST'], config['MPP']['USER'], config['MPP']['PASS'])

	def connectFTP(self):
		print("[ Connect MPP ]")
		print("FTP is connecting...")
		self.ftp = ''
		try:
			self.ftp = FTP(config['MPP']['HOST'], config['MPP']['USER'], config['MPP']['PASS'])
			print("FTP is connected !")
		except Exception as e:
			print("Can't connected !")
		print("============================================\n")

	# Upload MPP
	def uploadMPP(self):
		self.connectFTP()
		self.ftp.cwd('/')
		self.ftp.cwd(config['MPP']['MPP_PATH'])
		print("[ Upload MPP ]")
		try:
			for file in glob.glob(os.path.join(config['LOCAL']['MPP_PATH'], '*.xml')):
				originalFile = os.path.basename(file)
				print("Uploading: " + originalFile)
				file = open(file, 'rb')
				self.ftp.cwd('/')
				self.ftp.storbinary('STOR '+config['MPP']['MPP_PATH']+originalFile, file)
				file.close()
				self.ftp.cwd('/')
				shutil.move(config['LOCAL']['MPP_PATH']+originalFile, config['LOCAL']['MPP_OLD_PATH']+originalFile)
		except Exception as e:
			print(e)
		print('============================================\n')

	# Disconnect FTP
	def disconnectFTP(self):
		try:
			self.ftp.quit()
			print("MPP is disconnected")
		except ErrorValue:
			print("Can't disconnect !")
		print("============================================\n")
