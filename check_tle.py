import ephem
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv, verify_checksum, fix_checksum

tle1 = ('1 99918U 18777A   18314.89027777  .00000000  00000-0  10000-3 0   182', '2 99918   3.4992 208.8194 7280328 178.0950  13.0669  2.27879284   326')


tle1_fixed = (fix_checksum(tle1[0]), fix_checksum(tle1[1]))

sat1 = ephem.readtle("GK2A", tle1_fixed[0], tle1_fixed[1])

for tle in tle1_fixed:
    for line in tle:
        verify_checksum(line)

print(tle1)
print('-----------------')
print(tle1_fixed)
print(sat1)

